<?php

class M_Siswa extends CI_Model
{
    public function fetch_all()
    {
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('siswa');
        return $query->result_array();
    }

    public function fetch_single($id)
    {
        $this->db->where("id", $id);
        $query = $this->db->get('siswa');
        return $query->row();
    }

    public function insert_api($data)
    {
        $this->db->insert('siswa', $data);
        return $this->db->insert_id();
    }

    public function update_data($id, $data)
    {
        $this->db->where("id", $id);
        $this->db->update("siswa", $data);
    }

    public function check_data($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('siswa');
        return $query->row() ? true : false;
    }

    public function delete_data($id)
    {
        $this->db->where("id", $id);
        $this->db->delete("siswa");

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
?>
