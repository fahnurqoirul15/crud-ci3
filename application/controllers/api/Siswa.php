<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Siswa extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database(); // optional
        $this->load->model('M_Siswa');
        $this->load->library('form_validation');
    }  

    function fetch_all()
    {
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('siswa');
        return $query->result_array();
    }

    function fetch_single_data($id)
    {
        $this->db->where("id", $id);
        $query = $this->db->get('siswa');
        return $query->row();
    }

    function index_get()
    {
        $id = $this->get('id');
        if ($id === NULL) {
            $data = $this->fetch_all();
        } else {
            $data = $this->fetch_single_data($id);
        }

        $this->response($data, 200);
    }

    function check_data($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('siswa');

        if ($query->row()) {
            return true;
        } else {
            return false;
        }
    }

    function insert_api($data)
    {
        $this->db->insert('siswa', $data);
        if ($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    function index_post()
    {
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('class_id', 'Class ID', 'required');
        $this->form_validation->set_rules('date_birth', 'Date of Birth', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required|in_list[laki-laki,perempuan]');
        $this->form_validation->set_rules('address', 'Address', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = array(
                'status' => 'fail',
                'message' => validation_errors(),
                'status_code' => 422
            );
            return $this->response($error, 422);
        } else {
            $data = array(
                'name' => trim($this->post('name')),
                'class_id' => $this->post('class_id'),
                'date_birth' => $this->post('date_birth'),
                'gender' => $this->post('gender'),
                'address' => trim($this->post('address')),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );

            $this->insert_api($data);
            $last_row = $this->db->select('*')->order_by('id', 'desc')->limit(1)->get('siswa')->row();
            $response = array(
                'status' => 'success',
                'data' => $last_row,
                'status_code' => 201,
            );
            return $this->response($response, 201);
        }
    }

    function update_data($id, $data)
    {
        $this->db->where("id", $id);
        $this->db->update("siswa", $data);
    }

    function index_put() {
        $id = $this->put('id');
        $check = $this->M_Siswa->check_data($id);
    
        if ($check == false) {
            $error = array('status' => 'fail', 'field' => 'id', 'message' => 'id is not found', 'status_code' => 502);
            return $this->response($error);
        }
    
        if ($this->put('name') == '') {
            $response = array('status' => 'fail', 'field' => 'name', 'message' => 'name is required', 'status_code' => 502);
            return $this->response($response);
        }
    
        if ($this->put('date_birth') == '') {
            $response = array('status' => 'fail', 'field' => 'date_birth', 'message' => 'date_birth is required', 'status_code' => 502);
            return $this->response($response);
        }
    
        if ($this->put('gender') == '') {
            $response = array('status' => 'fail', 'field' => 'gender', 'message' => 'gender is required', 'status_code' => 502);
            return $this->response($response);
        }
    
        if ($this->put('address') == '') {
            $response = array('status' => 'fail', 'field' => 'address', 'message' => 'address is required', 'status_code' => 502);
            return $this->response($response);
        }
    
        $data = array(
            'name' => trim($this->put('name')),
            'date_birth' => trim($this->put('date_birth')),
            'gender' => trim($this->put('gender')),
            'address' => trim($this->put('address')),
        );
    
        $this->M_Siswa->update_data($id, $data);
        $newData = $this->M_Siswa->fetch_single($id);
        $response = array('status' => 'success', 'data' => $newData, 'status_code' => 200);
    
        return $this->response($response);
    }
    

    function index_delete() {
        $id = $this->delete('id');

        $check = $this->check_data($id);

        if (!$check) {
            $error = array(
                'status' => 'fail',
                'field' => 'id',
                'message' => 'Data tidak ditemukan!',
                'status_code' => 502
            );
            return $this->response($error);
        }

        $this->db->where('id', $id);
        $this->db->delete('siswa');

        $response = array(
            'status' => 'success',
            'data' => null,
            'status_code' => 200,
        );

        return $this->response($response);
    }
}
?>
