<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Kelas extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database(); // optional
        $this->load->model('M_Kelas');
        $this->load->library('form_validation');
    }  

    function fetch_all()
    {
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('kelas');
        return $query->result_array();
    }

    function fetch_single_data($id)
    {
        $this->db->where("id", $id);
        $query = $this->db->get('kelas');
        return $query->row();
    }

    function index_get()
    {
        $id = $this->get('id');
        if ($id === NULL) {
            $data = $this->fetch_all();
        } else {
            $data = $this->fetch_single_data($id);
        }

        $this->response($data, 200);
    }

    function check_data($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('kelas');

        if ($query->row()) {
            return true;
        } else {
            return false;
        }
    }

    function insert_api($data)
    {
        $this->db->insert('kelas', $data);
        if ($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    function index_post()
    {
        // Validation for class_code and class_name goes here if needed

        $data = array(
            'class_code' => trim($this->post('class_code')),
            'class_name' => trim($this->post('class_name')),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );

        $this->insert_api($data);
        $last_row = $this->db->select('*')->order_by('id', 'desc')->limit(1)->get('kelas')->row();
        $response = array(
            'status' => 'success',
            'data' => $last_row,
            'status_code' => 201,
        );
        return $this->response($response);
    }

    function update_data($id, $data)
    {
        $this->db->where("id", $id);
        $this->db->update("kelas", $data);
    }

    function index_put()
    {
        $id = $this->put('id');
        $check = $this->check_data($id);
        if ($check == false) {
            $error = array(
                'status' => 'fail',
                'field' => 'id',
                'message' => 'Data tidak ditemukan!',
                'status_code'=> 502
            );

            return $this->response($error);
        }

        $data = array(
            'class_code' => trim($this->put('class_code')),
            'class_name' => trim($this->put('class_name')),
            'updated_at' => date('Y-m-d H:i:s')
        );

        $this->update_data($id, $data);
        $newData = $this->fetch_single_data($id);
        $response = array(
            'status' => 'success',
            'data' => $newData,
            'status_code' => 200,
        );
        return $this->response($response);
    }

    function index_delete() {
        $id = $this->delete('id');
    
        $check = $this->check_data($id);
    
        if (!$check) {
            $error = array(
                'status' => 'fail',
                'field' => 'id',
                'message' => 'Data tidak ditemukan!',
                'status_code' => 502
            );
            return $this->response($error);
        }
    
        $this->db->where('id', $id);
        $this->db->delete('kelas');
    
        $response = array(
            'status' => 'success',
            'data' => null,
            'status_code' => 200,
        );
    
        return $this->response($response);
    }
    
}
?>
